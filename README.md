﻿Copyrights@vijay 2018
 
Steps to run test webserver

1) Pull the docker image by running the command: docker pull vijayks040/goibibo_test

2) Run the docker file using command: docker run DOCKER-IMAGE-NAME:latest

3) Web server will be running in 8080 port, please make sure no other applications are running on same port

4) Please use the shared Postman-API to test the webserver

POSTMAN-LINK: https://www.getpostman.com/collections/d3a1f5e9a2b1d94bf57f

Note: Make sure you have the Corpus.csv file inside csvFile folder